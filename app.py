from flask import Flask, render_template, request, redirect, url_for, session, flash
from flask_session import Session
import json

app = Flask(__name__)

@app.route("/")
def start():
    """ Page d'accueil redirigée vers les actualités """
    return redirect(url_for('actualite'))

@app.route("/actualites")
def actualite():
    """ Afficher les actualités """
    dataActualites=read_json("actualites")
    dataCommentaire=read_json("commentaire")
    return render_template("actualites.html", data=dataActualites, commentaire=dataCommentaire, name="all")

@app.route("/actualites/<name>")
def specific_actualite(name):
    """ Afficher les actualités par type """
    dataActualites=read_json("actualites")
    dataCommentaire=read_json("commentaire")
    return render_template("actualites.html", data=dataActualites, commentaire=dataCommentaire, name=name)

@app.route("/concert")
def concert():
    """ Afficher les concerts """
    dataActualites=read_json("concert")
    return render_template("concert.html", data=dataActualites)

@app.route("/commentaire", methods=["POST"])
def commentaire():
    """ Valider et écrire un commentaire """
    write_json("commentaire", {'actu': request.form['actu'], 'name': request.form['name'], 'commentaire': request.form['commentaire']})
    return redirect(url_for('actualite'))

def read_json(name):
    """ Lire dans un fichier Json """
    f = open(name + '.json')
    data = json.load(f)

    f.close()
    return data

def write_json(name, data):
    """ Ecrire dans un fichier Json existant """
    dataFromFile = read_json(name)
    f = open(name + '.json', "w")

    dataFromFile.append(data)

    f.write(json.dumps(dataFromFile))

    f.close()
    return data

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if username in USERS and USERS[username] == password:
            session['user'] = username
            flash('You were successfully logged in', 'success')
            return redirect(url_for('actualite'))
        else:
            flash('Invalid credentials. Please try again.', 'danger')
    return render_template('login.html')

@app.route('/logout')
def logout():
    session.pop('user', None)
    flash('You were successfully logged out', 'success')
    return redirect(url_for('login'))


# Clé secrète pour sécuriser les sessions
app.secret_key = 'your_secret_key'

# Configuration de Flask-Session
app.config['SESSION_TYPE'] = 'filesystem'
Session(app)

# Utilisateur fictif pour l'exemple
USERS = {
    'admin': 'password'  # Nom d'utilisateur : mot de passe
}

@app.route('/add_actualites', methods=['GET', 'POST'])
def add_actualite():
    if 'user' not in session:
        flash('You need to be logged in to add an actualité', 'danger')
        return redirect(url_for('login'))

    if request.method == 'POST':
        title = request.form['title']
        dateActu = request.form['dateActu']
        type_actu = request.form['type']
        
        new_id = max(item["id"] for item in read_json("actualites")) + 1
        new_actualite = {"id": new_id, "title": title, "dateActu": dateActu, "type": type_actu}
        
        write_json("actualites", new_actualite)
        
        flash('Actualité ajoutée avec succès!', 'success')
        return redirect(url_for('actualite'))
    
    return render_template('add_actualites.html')

@app.route('/liste_actualites')
def liste_actualites():
    if 'user' not in session:
        flash('You need to be logged in to view this page', 'danger')
        return redirect(url_for('login'))
    
    dataActualites = read_json("actualites")
    return render_template("liste_actualites.html", data=dataActualites)

@app.route('/delete_actualite/<int:id>', methods=['POST'])
def delete_actualite(id):
    if 'user' not in session:
        flash('You need to be logged in to perform this action', 'danger')
        return redirect(url_for('login'))
    
    dataActualites = read_json("actualites")
    dataActualites = [item for item in dataActualites if item["id"] != id]
    
    with open('actualites.json', 'w') as f:
        json.dump(dataActualites, f, indent=4)
    
    flash('Actualité supprimée avec succès!', 'success')
    return redirect(url_for('liste_actualites'))

def read_json(name):
    """ Lire dans un fichier Json """
    with open(name + '.json') as f:
        data = json.load(f)
    return data

def write_json(name, data):
    """ Ecrire dans un fichier Json existant """
    dataFromFile = read_json(name)
    with open(name + '.json', "w") as f:
        dataFromFile.append(data)
        f.write(json.dumps(dataFromFile, indent=4))
    return data

@app.route('/add_concert', methods=['GET', 'POST'])
def add_concert():
    if 'user' not in session:
        flash('You need to be logged in to add a concert', 'danger')
        return redirect(url_for('login'))

    if request.method == 'POST':
        title = request.form['title']
        dateConcert = request.form['dateConcert']

        new_id = max(item["id"] for item in read_json("concert")) + 1
        new_concert = {"id": new_id, "title": title, "dateConcert": dateConcert}
        
        write_json("concert", new_concert)
        
        flash('Concert ajouté avec succès!', 'success')
        return redirect(url_for('concert'))
    
    return render_template('add_concert.html')

@app.route('/liste_concerts')
def liste_concerts():
    if 'user' not in session:
        flash('You need to be logged in to view this page', 'danger')
        return redirect(url_for('login'))
    
    dataConcerts = read_json("concert")
    return render_template("liste_concerts.html", data=dataConcerts)

@app.route('/delete_concert/<int:id>', methods=['POST'])
def delete_concert(id):
    if 'user' not in session:
        flash('You need to be logged in to perform this action', 'danger')
        return redirect(url_for('login'))
    
    dataConcerts = read_json("concert")
    dataConcerts = [item for item in dataConcerts if item["id"] != id]
    
    with open('concert.json', 'w') as f:
        json.dump(dataConcerts, f, indent=4)
    
    flash('Concert supprimé avec succès!', 'success')
    return redirect(url_for('liste_concerts'))
